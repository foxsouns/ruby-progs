#!/usr/bin/env ruby
# print from 1 to 100, unless it is divisible by 3, where we print fizz, and divisible by 5, where we print buzz, and if both, fizzbuzz
count_to = 100
num = 1
while count_to > num
  if num % 3 == 0 and num % 5 == 0
    puts "FizzBuzz"
  elsif num % 3 == 0
    puts "Fizz"
  elsif num % 5 == 0
    puts "Buzz"
  else
    puts num
  end
  num = num + 1
end
