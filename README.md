# ruby practice scripts
this repo holds (or, at the moment, _will_ hold) a bunch of useless ruby scripts i make. everything here will be mit, unless i say otherwise.

### fizzbuzz.rb
whipped this one up really quick just to prove that i can. yes, i am aware that while sucks; no, i am not changing it right now.

### passgen.rb
a cute little password generator i whipped up to bip my toes into ruby. pretty finished tbh.

if i gain motivation to do so, i'll maybe put logic into this. maybe.

licensed under the mit license, except for [that stack overflow bit](https://stackoverflow.com/a/26220538), which is CC BY-SA 4.0.
