#!/usr/bin/env ruby
# vim: set ai ts=2 et sw=2 tw=80:

# this is shamelessly stolen from https://stackoverflow.com/a/26220538
# that means this bit is CC BY-SA 4.0. i logically expanded it,
# but this is totally not my thing lol. used for comedic effect, without
# going full down and installing a gem.
# ps: seems like it works just fine if you just add more numbers to make it 
# bigger. i also added that comment for -numbers, and it also just works.
def in_words(int)
  numbers_to_name = {
    1000000000000000000000000000000000 => "decillion",
    1000000000000000000000000000000 => "nonillion",
    1000000000000000000000000000 => "octillion",
    1000000000000000000000000 => "septillion",
    1000000000000000000000 => "sextillion",
    1000000000000000000 => "quintillion",
    1000000000000000 => "quadrillion",
    1000000000000 => "trillion",
    1000000000 => "billion",
    1000000 => "million",
    1000 => "thousand",
    100 => "hundred",
    90 => "ninety",
    80 => "eighty",
    70 => "seventy",
    60 => "sixty",
    50 => "fifty",
    40 => "forty",
    30 => "thirty",
    20 => "twenty",
    19 => "nineteen",
    18 => "eighteen",
    17 => "seventeen", 
    16 => "sixteen",
    15 => "fifteen",
    14 => "fourteen",
    13 => "thirteen",              
    12 => "twelve",
    11 => "eleven",
    10 => "ten",
    9 => "nine",
    8 => "eight",
    7 => "seven",
    6 => "six",
    5 => "five",
    4 => "four",
    3 => "three",
    2 => "two",
    1 => "one"
    }
  str = ""
  numbers_to_name.each do |num, name|
    if int == 0
      return str
    elsif int < 0
      str << 'negative '
      int = int * -1
    elsif int.to_s.length == 1 && int/num > 0
      return str + "#{name}"      
    elsif int < 100 && int/num > 0
      return str + "#{name}" if int%num == 0
      return str + "#{name} " + in_words(int%num)
    elsif int/num > 0
      return str + in_words(int/num) + " #{name} " + in_words(int%num)
    end
  end
end
# thus concludes our stack overflow rip-off intermission.

# anyways, welcome to the real code. some basic ruby stuff here.
puts"welcome to a garbage password generator, created by foxsouns. made without
logic, and maybe that's the way i like it.

this program is licensed under the MIT License. said license is available, if
you wish to see it. enter \"license\" (without quotes) for it to be printed to
console, or press enter to continue."

license = gets.chomp.downcase
if license == "license" ; puts'
Begin license text.

Copyright 2021 foxsouns

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

End license text.'
puts"\npress enter when you are done readin'."
gets
end

# this lil chunk puts stuff, takes your input, and puts it in a variable;
# unless you enter nothing, in which case, we give it a value. then, we
# count how many characters are in the input, and stick that in another var.

# ps: i make the charlist variable here because convenience the 80 char rule.
defaultcharlist = "`1234567890-=qwertyuiop[]\asdfghjkl;\\'zxcvbnm,./ ~!@\#$%^&*(\
)_+QWERTYUIOP{}|ASDFGHJKL:\"ZXCVBNM<>?"
puts"so, what characters do you want? the default is the following nonsense,
which happens to be every valid character on the us english keyboard.\n" +
defaultcharlist
pwchars = gets.chomp.to_s
if pwchars == "" ; pwchars = defaultcharlist end
pwcharsnum = pwchars.size

# pretty self descriptive. take a number, turn it into an int, and have a
# default ready if we need it.
puts "min chars in ur pw? default value is 26."
pwmin = gets.chomp.to_i
if pwmin == 0 ; pwmin = 26 end

puts "max chars in your pw? defaults to 32."
pwmax = gets.chomp.to_i
if pwmax == 0 ; pwmax = 32 end

# same story as above.
puts "how many passwords do you want? default is 20."
pwtot = gets.chomp.to_i
if pwtot == 0 ; pwtot = 20 end

ex = pwmin
ez = pwmax
cpar=[*ex..ez]
yz = pwtot
password = ""
puts "passwords are on the way! here they come:"
while yz > 0
  # i need to make an array of the numbers inbetween pwmin and pwmax,
  # and use it for the compare variable in the below while.
  compar = cpar[rand(ez-ex)]
  while compar > 0
    password << pwchars.chars[rand(pwcharsnum)]
    compar = compar - 1
  end
  ex = pwmin
  puts password
  password = ""
  yz = yz - 1
end

puts"\ni just generated "+in_words(pwtot)+" entire passwords for you, each \
bearing "+in_words(pwmin)+" to "+in_words(pwmax)+" whole characters. i hope you're happy, as \
that's about all that i can do."

puts "\nalso, my favorite number right now is "+in_words(rand(('1'+('0'*33)).to_i))+".
thank you for listening to my ted talk."
